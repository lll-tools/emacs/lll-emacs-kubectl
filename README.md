# Introduction

GNU Emacs transient mode for `kubectl`  


# Quick Start

Git clone this repository  

```shell
cd /usr/local/src
git clone https://gitlab.com/lll-tools/emacs/lll-emacs-kubectl.git
```

Add the `src` directory to the GNU Emacs `load-path` variable.  

```emacs-lisp
(add-to-list 'load-path
	     (expand-file-name
	      "/usr/local/src/lll-emacs-kubectl/src"))

(when (y-or-n-p "Load lll-kubectl module? ")
  (require 'lll-kubectl)
  (global-set-key (kbd "C-c k") 'lll-kubectl))
```


# Description

Manage kubernetes clusters  


## Configuration

`C-c k` , `c`  
List kubectl contexts  


## Nodes

`C-c k` , `n`  
List cluster nodes  

```shell
kubectl get nodes
```


## Pods

`C-c k` , `p`  
List cluster pods  


## Deployments

`C-c k` , `d`  
List cluster deployments  


## Services


## Secrets

`C-c k` , `s`  
List cluster secrets  

-   Decode secret  
    
    ```shell
    kubectl get secret default-server-secret \
    	-n nginx-ingress \
    	--template="{{index .data \"tls.crt\" | base64decode}}"
    ```


## Persistent Volumes


## Persistent Volume Claims


## ReplicaSets


## DaemonSets


## StateFulSets

`C-c k` , `t`  
List cluster statefulsets
