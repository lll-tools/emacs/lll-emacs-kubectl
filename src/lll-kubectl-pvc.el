;;; lll-kubectl.el --- pvc file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-pvc-selected ""
  "kubectl pvc selected")

(defun lll-kubectl-pvc-log ()
  "Log kubectl pvc"
  (interactive)
  (let* ((lll-kubectl-pvc-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pvc-selected (car lll-kubectl-pvc-selected-list))
	 (lll-kubectl-pvc-selected-name (car lll-kubectl-pvc-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pvc-log*" nil
		    ;; (concat "lll-kubectl-pvc-describe-" lll-kubectl-pvc-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "pvc/" lll-kubectl-pvc-selected-name)
		    )))
  )

(defun lll-kubectl-pvc-describe ()
  "Describe kubectl pvc"
  (interactive)
  (let* ((lll-kubectl-pvc-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pvc-selected (car lll-kubectl-pvc-selected-list))
	 (lll-kubectl-pvc-selected-name (car lll-kubectl-pvc-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pvc-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "pvc"
		    lll-kubectl-pvc-selected-name
		    )))
  )

;; PVC definition -----------------------------------------------
(define-transient-command lll-kubectl-pvc-help ()
  "Help transient for lll kubectl pvc mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log pvc"	lll-kubectl-pvc-log)
   ("d" "Describe pvc"	lll-kubectl-pvc-describe)]
  )

(defvar lll-kubectl-pvc-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-pvc-help)
    (define-key map "l" 'lll-kubectl-pvc-log)
    (define-key map "d" 'lll-kubectl-pvc-describe)
    map)
  "Keymap for `lll-kubectl-pvc-mode'")

(defun lll-kubectl-pvc-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Pending" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Bound" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-pvc-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-pvc-entry (split-string line))
      	     (lll-pvc-entry-length (length lll-pvc-entry))
	     )
	;; (when (< lll-pvc-entry-length 7)
	;;   (setq num 0)
	;;   ;; (while (< num missing-elems)
	;;   ;;   (add-to-ordered-list 'lll-pvc-entry '"-" (- lll-pvc-entry-length 1))
	;;   ;;   (setq num (1+ num)))
	;;   )
	(setq missing-elems (- 7 lll-pvc-entry-length))
	;; (setq lll-pvc-entry2 (append lll-pvc-entry (make-list missing-elems '"-")))
	;; (setq lll-pvc-entry-vector (vconcat (lll-pvc-entry2)))
	(setq lll-pvc-entry-vector
	      (vconcat
	       (append (butlast lll-pvc-entry 2)
		       (make-list missing-elems '"-")
		       (nthcdr (- lll-pvc-entry-length 2) lll-pvc-entry)
		       )
	       )
	      )
	(setq lll-pvc-status (aref lll-pvc-entry-vector 1))
	(aset lll-pvc-entry-vector 1
	      (propertize lll-pvc-status
			  'font-lock-face (lll-kubectl-pvc-status-face lll-pvc-status)))
      	(list (aref lll-pvc-entry-vector 0) lll-pvc-entry-vector))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-pvc-entries ()
  "Return the kubectl pvcs data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get pvc"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-pvc-parse lines)))

(defun lll-kubectl-pvc-refresh ()
  "Refresh kubectl pvc list"
  (setq tabulated-list-entries (lll-kubectl-pvc-entries)))

;; (defcustom lll-kubectl-pvc-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "STATUS")
;;                        (const "VOLUME")
;;                        (const "CAPACITY")
;;                        (const "ACCESS MODES")
;;                        (const "STORAGECLASS")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-pvc-mode tabulated-list-mode
  "LLL kubectl pvcs"
  "Major mode for handling pvcs"
  (setq tabulated-list-format [("NAME" 14 t)
			       ("STATUS" 8 t)
			       ("VOLUME" 14 t)
			       ("CAPACITY" 10 t)
			       ("ACCESS MODES" 14 t)
			       ("STORAGECLASS" 14 t)
			       ("AGE" 8 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-pvc-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-pvc ()
  "kubectl pvc list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-pvc*")
  (lll-kubectl-pvc-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-pvc-default-sort-key)
  (tabulated-list-revert))
;; PVC definition -----------------------------------------------

(provide 'lll-kubectl-pvc)

;;; lll-kubectl-pvc.el ends here
