;;; lll-kubectl.el --- dummy file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-dummy-selected ""
  "kubectl dummy selected")

(defun lll-kubectl-dummy-log ()
  "Log kubectl dummy"
  (interactive)
  (let* ((lll-kubectl-dummy-selected-list (tablist-get-marked-items))
	 (lll-kubectl-dummy-selected (car lll-kubectl-dummy-selected-list))
	 (lll-kubectl-dummy-selected-name (car lll-kubectl-dummy-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-dummy-log*" nil
		    ;; (concat "lll-kubectl-dummy-describe-" lll-kubectl-dummy-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "dummy/" lll-kubectl-dummy-selected-name)
		    )))
  )

(defun lll-kubectl-dummy-describe ()
  "Describe kubectl dummy"
  (interactive)
  (let* ((lll-kubectl-dummy-selected-list (tablist-get-marked-items))
	 (lll-kubectl-dummy-selected (car lll-kubectl-dummy-selected-list))
	 (lll-kubectl-dummy-selected-name (car lll-kubectl-dummy-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-dummy-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "dummy"
		    lll-kubectl-dummy-selected-name
		    )))
  )

(defun lll-kubectl-dummy-yaml ()
  "Get kubectl dummy yaml"
  (interactive)
  (let* ((lll-kubectl-dummy-selected-list (tablist-get-marked-items))
	 (lll-kubectl-dummy-selected (car lll-kubectl-dummy-selected-list))
	 (lll-kubectl-dummy-selected-name (car lll-kubectl-dummy-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-dummy-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "get" "dummy"
		    lll-kubectl-dummy-selected-name
		    "-o" "yaml"
		    )))
  )

;; DUMMY definition -----------------------------------------------
(define-transient-command lll-kubectl-dummy-help ()
  "Help transient for lll kubectl dummy mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log dummy"		lll-kubectl-dummy-log)
   ("d" "Describe dummy"	lll-kubectl-dummy-describe)
   ("y" "Get dummy yaml"	lll-kubectl-dummy-yaml)
   ]
  )

(defvar lll-kubectl-dummy-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-dummy-help)
    (define-key map "l" 'lll-kubectl-dummy-log)
    (define-key map "d" 'lll-kubectl-dummy-describe)
    (define-key map "y" 'lll-kubectl-dummy-yaml)
    map)
  "Keymap for `lll-kubectl-dummy-mode'")

(defun lll-kubectl-dummy-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-dummy-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-dummy-entry (vconcat (split-string line)))
      	     (lll-dummy-entry-length (length lll-dummy-entry))
	     (lll-dummy-status (aref lll-dummy-entry 1)))
	(aset lll-dummy-entry 1
	      (propertize lll-dummy-status
			  'font-lock-face (lll-kubectl-dummy-status-face lll-dummy-status)))
      	(list (aref lll-dummy-entry 0) lll-dummy-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-dummy-entries ()
  "Return the kubectl dummys data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get dummys"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-dummy-parse lines)))

(defun lll-kubectl-dummy-refresh ()
  "Refresh kubectl dummy list"
  (setq tabulated-list-entries (lll-kubectl-dummy-entries)))

;; (defcustom lll-kubectl-dummy-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "STATUS")
;;                        (const "ROLES")
;;                        (const "AGE")
;;                        (const "VERSION")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-dummy-mode tabulated-list-mode
  "LLL kubectl dummys"
  "Major mode for handling dummys"
  (setq tabulated-list-format [("NAME" 25 t)
			       ("STATUS" 8 t)
			       ("ROLES" 25 t)
			       ("AGE" 5 t)
			       ("VERSION" 10 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-dummy-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-dummy ()
  "kubectl dummy list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-dummy*")
  (lll-kubectl-dummy-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-dummy-default-sort-key)
  (tabulated-list-revert))
;; DUMMY definition -----------------------------------------------

(provide 'lll-kubectl-dummy)

;;; lll-kubectl-dummy.el ends here
