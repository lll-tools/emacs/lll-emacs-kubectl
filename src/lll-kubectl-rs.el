;;; lll-kubectl.el --- rs file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-rs-selected ""
  "kubectl rs selected")

(defun lll-kubectl-rs-log ()
  "Log kubectl rs"
  (interactive)
  (let* ((lll-kubectl-rs-selected-list (tablist-get-marked-items))
	 (lll-kubectl-rs-selected (car lll-kubectl-rs-selected-list))
	 (lll-kubectl-rs-selected-name (car lll-kubectl-rs-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-rs-log*" nil
		    ;; (concat "lll-kubectl-rs-describe-" lll-kubectl-rs-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "rs/" lll-kubectl-rs-selected-name)
		    )))
  )

(defun lll-kubectl-rs-describe ()
  "Describe kubectl rs"
  (interactive)
  (let* ((lll-kubectl-rs-selected-list (tablist-get-marked-items))
	 (lll-kubectl-rs-selected (car lll-kubectl-rs-selected-list))
	 (lll-kubectl-rs-selected-name (car lll-kubectl-rs-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-rs-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "rs"
		    lll-kubectl-rs-selected-name
		    )))
  )

;; RS definition -----------------------------------------------
(define-transient-command lll-kubectl-rs-help ()
  "Help transient for lll kubectl rs mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log rs"	lll-kubectl-rs-log)
   ("d" "Describe rs"	lll-kubectl-rs-describe)]
  )

(defvar lll-kubectl-rs-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-rs-help)
    (define-key map "l" 'lll-kubectl-rs-log)
    (define-key map "d" 'lll-kubectl-rs-describe)
    map)
  "Keymap for `lll-kubectl-rs-mode'")

(defun lll-kubectl-rs-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-rs-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-rs-entry (vconcat (split-string line)))
      	     (lll-rs-entry-length (length lll-rs-entry))
	     (lll-rs-status (aref lll-rs-entry 1)))
	(aset lll-rs-entry 1
	      (propertize lll-rs-status
			  'font-lock-face (lll-kubectl-rs-status-face lll-rs-status)))
      	(list (aref lll-rs-entry 0) lll-rs-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-rs-entries ()
  "Return the kubectl rss data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get rs"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-rs-parse lines)))

(defun lll-kubectl-rs-refresh ()
  "Refresh kubectl rs list"
  (setq tabulated-list-entries (lll-kubectl-rs-entries)))

;; (defcustom lll-kubectl-rs-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "DESIRED")
;;                        (const "CURRENT")
;;                        (const "READY")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-rs-mode tabulated-list-mode
  "LLL kubectl rss"
  "Major mode for handling rss"
  (setq tabulated-list-format [("NAME" 40 t)
			       ("DESIRED" 8 t)
			       ("CURRENT" 8 t)
			       ("READY" 8 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-rs-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-rs ()
  "kubectl rs list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-rs*")
  (lll-kubectl-rs-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-rs-default-sort-key)
  (tabulated-list-revert))
;; RS definition -----------------------------------------------

(provide 'lll-kubectl-rs)

;;; lll-kubectl-rs.el ends here
