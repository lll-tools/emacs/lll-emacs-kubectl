;;; lll-kubectl.el --- pod file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-pod-selected ""
  "kubectl pod selected")

(defun lll-kubectl-pod-forward ()
  "Forward port kubectl pod"
  (interactive)
  (let* ((lll-kubectl-pod-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pod-selected (car lll-kubectl-pod-selected-list))
	 (lll-kubectl-pod-selected-name (car lll-kubectl-pod-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pod-forward*" nil
		    ;; (concat "lll-kubectl-pod-describe-" lll-kubectl-pod-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "port-forward"
		    lll-kubectl-pod-selected-name
		    (concat ":" (lll-kubectl-run-command
				 "-n" "ce-cep-mp" "get" "pod"
				 lll-kubectl-pod-selected-name
				 "-o" "jsonpath='{.spec.containers[?(@.name==\"prometheus\")].ports[?(@.name==\"web\")].containerPort}'"
				 ;; "--template='{{(index (index .spec.containers 0).ports 0).containerPort}}'"
				 ))
		    ;; ":9090"
		    ;; (concat lll-kubectl-pod-getport ":")
		    )))
  )

(defun lll-kubectl-pod-getport ()
  "Forward port kubectl pod"
  (interactive)
  (let* ((lll-kubectl-pod-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pod-selected (car lll-kubectl-pod-selected-list))
	 (lll-kubectl-pod-selected-name (car lll-kubectl-pod-selected))
	 )

    ;; kubectl -n ce-cep-mp get po prometheus-monitoring-prometheus-0 -o jsonpath='{.spec.containers[?(@.name=="prometheus")].ports[?(@.name=="web")].containerPort}'

    (message (lll-kubectl-run-command
	      (car (split-string lll-kubectl-arguments))
	      (car (cdr (split-string lll-kubectl-arguments)))
	      "get" "pod"
	      lll-kubectl-pod-selected-name
	      "-o" "jsonpath='{.spec.containers[?(@.name==\"prometheus\")].ports[?(@.name==\"web\")].containerPort}'"
	      ;; "--template='{{(index (index .spec.containers 0).ports 0).containerPort}}'"
     )))
  )

(defun lll-kubectl-pod-term ()
  "Term kubectl pod"
  (interactive)
  (let* ((lll-kubectl-pod-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pod-selected (car lll-kubectl-pod-selected-list))
	 (lll-kubectl-pod-selected-name (car lll-kubectl-pod-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pod-term*" nil
		    ;; (concat "lll-kubectl-pod-describe-" lll-kubectl-pod-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "exec" "-it"
		    lll-kubectl-pod-selected-name
		    "--" "/bin/sh"
		    )))
  )

(defun lll-kubectl-pod-log ()
  "Log kubectl pod"
  (interactive)
  (let* ((lll-kubectl-pod-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pod-selected (car lll-kubectl-pod-selected-list))
	 (lll-kubectl-pod-selected-name (car lll-kubectl-pod-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pod-log*" nil
		    ;; (concat "lll-kubectl-pod-describe-" lll-kubectl-pod-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f" "--since=5m"
		    lll-kubectl-pod-selected-name
		    )))
  )

(defun lll-kubectl-pod-describe ()
  "Describe kubectl pod"
  (interactive)
  (let* ((lll-kubectl-pod-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pod-selected (car lll-kubectl-pod-selected-list))
	 (lll-kubectl-pod-selected-name (car lll-kubectl-pod-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pod-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "pod"
		    lll-kubectl-pod-selected-name
		    )))
  )

(defun lll-kubectl-pod-yaml ()
  "Get kubectl pod yaml"
  (interactive)
  (let* ((lll-kubectl-pod-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pod-selected (car lll-kubectl-pod-selected-list))
	 (lll-kubectl-pod-selected-name (car lll-kubectl-pod-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pod-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "get" "pod"
		    lll-kubectl-pod-selected-name
		    "-o" "yaml"
		    )))
  )

;; POD definition -----------------------------------------------
(define-transient-command lll-kubectl-pod-help ()
  "Help transient for lll kubectl pod mode"
  [:description lll-kubectl-heading-actions
   ("?" "Describe mode"		describe-mode)
   ("l" "Log pod"		lll-kubectl-pod-log)
   ("t" "Open terminal in pod"	lll-kubectl-pod-term)
   ("f" "Forward port in pod"	lll-kubectl-pod-forward)
   ("d" "Describe pod"		lll-kubectl-pod-describe)
   ("y" "Get pod yaml"		lll-kubectl-pod-yaml)]
  )

(defvar lll-kubectl-pod-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-pod-help)
    (define-key map "a" 'lll-kubectl-pod-keys)
    (define-key map "l" 'lll-kubectl-pod-log)
    (define-key map "t" 'lll-kubectl-pod-term)
    (define-key map "f" 'lll-kubectl-pod-forward)
    (define-key map "d" 'lll-kubectl-pod-describe)
    (define-key map "y" 'lll-kubectl-pod-yaml)
    map)
  "Keymap for `lll-kubectl-pod-mode'")

(defun lll-kubectl-pod-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Running" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-pod-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-pod-entry (vconcat (split-string line)))
      	     (lll-pod-entry-length (length lll-pod-entry))
	     (lll-pod-status (aref lll-pod-entry 2))
	     (lll-pod-restarts (aref lll-pod-entry 3))
	     (lll-pod-age (aref lll-pod-entry 4)))
	(aset lll-pod-entry 2 (propertize lll-pod-status 'font-lock-face (lll-kubectl-pod-status-face lll-pod-status)))
	(aset lll-pod-entry 3 (format "%04d" (string-to-number lll-pod-restarts)))
	(aset lll-pod-entry 4 (lll-kubectl-age2hours lll-pod-age))
	;; (lll-kubectl-age2hours "1d1h")
      	(list (aref lll-pod-entry 0) lll-pod-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-pod-entries ()
  "Return the kubectl pods data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get pods"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-pod-parse lines)))

(defun lll-kubectl-pod-refresh ()
  "Refresh kubectl pod list"
  (setq tabulated-list-entries (lll-kubectl-pod-entries)))

;; (defcustom lll-kubectl-pod-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "READY")
;;                        (const "STATUS")
;;                        (const "RESTARTS")
;;                        (const "AGE"))
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-pod-mode tabulated-list-mode
  "LLL kubectl pods"
  "Major mode for handling pods"
  (setq tabulated-list-format [("NAME" 65 t)
			       ("READY" 5 t)
			       ("STATUS" 18 t)
			       ("RESTARTS" 10 t)
			       ("AGE" 18 t)
			       ])
  (setq tabulated-list-padding 2)
  ;; (setq tabulated-list-sort-key lll-kubectl-pod-default-sort-key)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-pod-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-pod ()
  "kubectl pod list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))
    ;; TODO
    ;; (let ((first-argument (car arguments))
    ;; 	  )
    ;;   (cond
    ;;    ((s-contains? "-n" first-argument)
    ;; 	(setq lll-kubectl-namespace (cdr (split-string first-argument))))
    ;;    (t
    ;; 	(setq lll-kubectl-namespace "kube-system")))
    ;;   ))

  (pop-to-buffer "*lll-kubectl-pod*")
  (lll-kubectl-pod-mode)
  (hl-line-mode)
  (tabulated-list-revert))
;; POD definition -----------------------------------------------

(provide 'lll-kubectl-pod)

;;; lll-kubectl-pod.el ends here
