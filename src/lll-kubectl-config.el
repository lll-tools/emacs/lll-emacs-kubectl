;;; lll-kubectl-config.el --- config file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(defvar lll-kubectl-config-context ""
  "Selected context of kubectl config.")

(defun lll-kubectl-config-keys-parse (line)
  "List all the keys and values for selected LINE config."
  (condition-case nil
      (let* ((command (concat lll-kubectl-command
			      " describe -n "
			      lll-kubectl-config-selected
			      " "
			      line))
	     (lll-kubectl-config-key-value
	      (replace-regexp-in-string "\n\\'" ""
		    (shell-command-to-string command))))
	(list line (vector
		    lll-kubectl-config-selected
		    line lll-kubectl-config-key-value)))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-config-keys-entries ()
  "Return the kubectl config keys data for `tabulated-list-entries' entry."
  (let* ((command (concat lll-kubectl-command
			  " api-versions "
			  lll-kubectl-config-selected
			  ))
	 (data (shell-command-to-string command))
	 (lines (split-string data)))
    (-map #'lll-kubectl-config-keys-parse lines)))

(defun lll-kubectl-config-keys-refresh ()
  "Refresh kubectl config keys list"
  (setq tabulated-list-entries (lll-kubectl-config-keys-entries)))

(define-derived-mode lll-kubectl-config-keys-mode tabulated-list-mode
  "Mode kubectl config keys list"
  "Major mode for handling GTK config keys"
  (setq tabulated-list-format [("Config" 35 t)
			       ("Key" 25 t)
			       ("Value" 15 t)])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-config-keys-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

(defun lll-kubectl-config-keys ()
  "Show kubectl config keys"
  (interactive)
  (setq lll-kubectl-config-selected (car (lll-kubectl-get-marked-items)))
  (pop-to-buffer "*lll-kubectl-config-keys*")
  (lll-kubectl-config-keys-mode)
  (tabulated-list-revert))

(defun lll-kubectl-config-current ()
  "Describe kubectl config"
  (interactive)
  (let* ((lll-kubectl-config-selected-list (tablist-get-marked-items))
	 (lll-kubectl-config-selected (car lll-kubectl-config-selected-list))
	 (lll-kubectl-config-selected-name (car lll-kubectl-config-selected))
	 (lll-kubectl-config-selected-cluster (elt (cdr lll-kubectl-config-selected) 2))
	 )
    (setq lll-kubectl-config-context lll-kubectl-config-selected-cluster)
    (setq command (concat lll-kubectl-command " config use-context " lll-kubectl-config-selected-name))
    (message command)
    (message (s-trim-right (shell-command-to-string command)))
    ;; (pop-to-buffer (make-comint-in-buffer
    ;; 		    "lll-kubectl-config-current" nil
    ;; 		    ;; (concat "lll-kubectl-config-current-" lll-kubectl-config-selected-name) nil
    ;; 		    lll-kubectl-command nil
    ;; 		    "config" "use-context" lll-kubectl-config-selected-name
    ;; 		    ))
    )
  (tablist-revert)
  )

(defun lll-kubectl-config-apiv ()
  "List kubectl API versions"
  (interactive)
  ;; (--each (lll-kubectl-get-marked-items-ids)
  ;;   (lll-kubectl-run-command "api-versions" it))
  (pop-to-buffer (make-comint-in-buffer
		  "lll-kubectl-config-apiv" nil
		  ;; (concat "lll-kubectl-config-current-" lll-kubectl-config-selected-name) nil
		  lll-kubectl-command nil
		  ;;  lll-kubectl-config-selected-name
		  "api-versions"
		  ))
  )

(defun lll-kubectl-config-apir ()
  "List kubectl API resources"
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "lll-kubectl-config-apir" nil
		  ;; (concat "lll-kubectl-config-current-" lll-kubectl-config-selected-name) nil
		  lll-kubectl-command nil
		  ;;  lll-kubectl-config-selected-name
		  "api-resources"
		  ))
  )

(defun lll-kubectl-config-info ()
  "Cluster info kubectl"
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "lll-kubectl-config-info" nil
		  ;; (concat "lll-kubectl-config-current-" lll-kubectl-config-selected-name) nil
		  lll-kubectl-command nil
		  ;;  lll-kubectl-config-selected-name
		  "cluster-info"
		  ))
  )

(defun lll-kubectl-config-info-dump ()
  "Cluster info DUMP kubectl"
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "lll-kubectl-config-info-dump" nil
		  ;; (concat "lll-kubectl-config-current-" lll-kubectl-config-selected-name) nil
		  lll-kubectl-command nil
		  ;;  lll-kubectl-config-selected-name
		  "cluster-info" "dump"
		  ))
  )

;; CONFIG definition -----------------------------------------------
(define-transient-command lll-kubectl-config-help ()
  "Help transient for lll kubectl config mode"
  ["LLL kubectl config help"
   ;; ("?" "Describe mode"		describe-mode)
   ("c" "Set current context"		lll-kubectl-config-current)
   ("i" "Current cluster info"		lll-kubectl-config-info)
   ("d" "Current cluster info dump"	lll-kubectl-config-info-dump)
   ("l" "List config keys"		lll-kubectl-config-keys)
   ("v" "List api versions"		lll-kubectl-config-apiv)
   ("r" "List api resources"		lll-kubectl-config-apir)
   ])

(defvar lll-kubectl-config-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-config-help)
    (define-key map "c" 'lll-kubectl-config-current)
    (define-key map "i" 'lll-kubectl-config-info)
    (define-key map "d" 'lll-kubectl-config-info-dump)
    (define-key map "l" 'lll-kubectl-config-keys)
    (define-key map "v" 'lll-kubectl-config-apiv)
    (define-key map "r" 'lll-kubectl-config-apir)
    map)
  "Keymap for `lll-kubectl-config-mode'")

(defun lll-kubectl-config-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "*" status)
    'lll-kubectl-face-status-up)
   (t
    'default)))

(defun lll-kubectl-config-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-config-context (split-string line))
	     (lll-config-context-length (length lll-config-context)))
	(when (= lll-config-context-length 4)
	  (if (string= (elt lll-config-context 0) "*")
	      (add-to-list 'lll-config-context '"-" t)
	    (push '"-" lll-config-context)))
	(when (= lll-config-context-length 3)
	  (add-to-list 'lll-config-context '"-" t)
	  (push '"-" lll-config-context))
	(setq lll-config-context-vector (vconcat lll-config-context))
	(setq lll-config-context-current (aref lll-config-context-vector 0))
	(setq lll-config-context-name (aref lll-config-context-vector 1))
	(aset lll-config-context-vector 1
	      (propertize lll-config-context-name
     			  'font-lock-face
     			  (lll-kubectl-config-status-face lll-config-context-current)))
	(list (aref lll-config-context-vector 1) lll-config-context-vector))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-config-entries ()
  "Return the kubectl configs data for `tabulated-list-entries' entry"
  (let* ((command (concat lll-kubectl-command " config get-contexts"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-config-parse lines)))

(defun lll-kubectl-config-refresh ()
  "Refresh kubectl config list"
  (setq tabulated-list-entries (lll-kubectl-config-entries)))


;; (defcustom lll-kubectl-config-default-sort-key '("CURRENT" . nil)
;;   "Sort key for kubectl contexts.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "CURRENT")
;;                        (const "NAME")
;;                        (const "CLUSTER")
;;                        (const "AUTHINFO")
;;                        (const "NAMESPACE"))
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-config-mode tabulated-list-mode
  "LLL-kubectl-config"
  "Major mode for handling configs"
  :group 'lll-kubectl
  (setq tabulated-list-format [("CURRENT" 8 t)
			       ("NAME" 35 t)
			       ("CLUSTER" 15 t)
			       ("AUTHINFO" 15 t)
			       ("NAMESPACE" 10 t)
			       ])
  (setq tabulated-list-padding 2)
  (setq tabulated-list-sort-key (cons "CURRENT" nil))
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-config-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-config ()
  "Config kubectl list."
  (interactive)
  (pop-to-buffer "*lll-kubectl-config*")
  (lll-kubectl-config-mode)
  (tabulated-list-revert)
  )

;;;###autoload
(defun lll-kubectl-dash ()
  "Config kubectl Dashboard."
  (interactive)
  (pop-to-buffer "*lll-kubectl-config*")
  (lll-kubectl-config-mode)
  ;; Aspect
  (text-scale-increase 2)
  ;; (hl-line-mode)
  ;; (set-face-background hl-line-face "white")
  (tabulated-list-revert)
  ;; Split view
  (delete-other-windows)
  (split-window-below 10)
  (other-window 1)
  (split-window-right)
  (switch-to-prev-buffer)
  (other-window 1)
  (switch-to-prev-buffer)
  (other-window 1)
  ;;(lll-kubectl-config-apir)
  (lll-kubectl-pod)
  )
;; CONFIG definition -----------------------------------------------

(provide 'lll-kubectl-config)

;;; lll-kubectl-config.el ends here
