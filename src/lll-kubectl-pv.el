;;; lll-kubectl.el --- pv file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-pv-selected ""
  "kubectl pv selected")

(defun lll-kubectl-pv-log ()
  "Log kubectl pv"
  (interactive)
  (let* ((lll-kubectl-pv-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pv-selected (car lll-kubectl-pv-selected-list))
	 (lll-kubectl-pv-selected-name (car lll-kubectl-pv-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pv-log*" nil
		    ;; (concat "lll-kubectl-pv-describe-" lll-kubectl-pv-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "pv/" lll-kubectl-pv-selected-name)
		    )))
  )

(defun lll-kubectl-pv-describe ()
  "Describe kubectl pv"
  (interactive)
  (let* ((lll-kubectl-pv-selected-list (tablist-get-marked-items))
	 (lll-kubectl-pv-selected (car lll-kubectl-pv-selected-list))
	 (lll-kubectl-pv-selected-name (car lll-kubectl-pv-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-pv-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "pv"
		    lll-kubectl-pv-selected-name
		    )))
  )

;; PV definition -----------------------------------------------
(define-transient-command lll-kubectl-pv-help ()
  "Help transient for lll kubectl pv mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log pv"	lll-kubectl-pv-log)
   ("d" "Describe pv"	lll-kubectl-pv-describe)]
  )

(defvar lll-kubectl-pv-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-pv-help)
    (define-key map "l" 'lll-kubectl-pv-log)
    (define-key map "d" 'lll-kubectl-pv-describe)
    map)
  "Keymap for `lll-kubectl-pv-mode'")

(defun lll-kubectl-pv-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-pv-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-pv-entry (vconcat (split-string line)))
      	     (lll-pv-entry-length (length lll-pv-entry))
	     (lll-pv-status (aref lll-pv-entry 1)))
	(aset lll-pv-entry 1
	      (propertize lll-pv-status
			  'font-lock-face (lll-kubectl-pv-status-face lll-pv-status)))
      	(list (aref lll-pv-entry 0) lll-pv-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-pv-entries ()
  "Return the kubectl pvs data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get pv"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-pv-parse lines)))

(defun lll-kubectl-pv-refresh ()
  "Refresh kubectl pv list"
  (setq tabulated-list-entries (lll-kubectl-pv-entries)))

;; (defcustom lll-kubectl-pv-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "CAPACITY")
;;                        (const "ACCESS MODES")
;;                        (const "RECLAIM POLICY")
;;                        (const "STATUS")
;;                        (const "CLAIM")
;;                        (const "STORAGECLASS")
;;                        ;; (const "REASON")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-pv-mode tabulated-list-mode
  "LLL kubectl pvs"
  "Major mode for handling pvs"
  (setq tabulated-list-format [("NAME" 20 t)
			       ("CAPACITY" 8 t)
			       ("ACCESS MODES" 14 t)
			       ("RECLAIM POLICY" 14 t)
			       ("STATUS" 10 t)
			       ("CLAIM" 20 t)
			       ("STORAGECLASS" 14 t)
			       ;; ("REASON" 18 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-pv-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-pv ()
  "kubectl pv list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-pv*")
  (lll-kubectl-pv-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-pv-default-sort-key)
  (tabulated-list-revert))
;; PV definition -----------------------------------------------

(provide 'lll-kubectl-pv)

;;; lll-kubectl-pv.el ends here
