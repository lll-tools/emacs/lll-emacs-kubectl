;;; lll-kubectl.el --- hpa file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-hpa-selected ""
  "kubectl hpa selected")

(defun lll-kubectl-hpa-log ()
  "Log kubectl hpa"
  (interactive)
  (let* ((lll-kubectl-hpa-selected-list (tablist-get-marked-items))
	 (lll-kubectl-hpa-selected (car lll-kubectl-hpa-selected-list))
	 (lll-kubectl-hpa-selected-name (car lll-kubectl-hpa-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-hpa-log*" nil
		    ;; (concat "lll-kubectl-hpa-describe-" lll-kubectl-hpa-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "hpa/" lll-kubectl-hpa-selected-name)
		    )))
  )

(defun lll-kubectl-hpa-describe ()
  "Describe kubectl hpa"
  (interactive)
  (let* ((lll-kubectl-hpa-selected-list (tablist-get-marked-items))
	 (lll-kubectl-hpa-selected (car lll-kubectl-hpa-selected-list))
	 (lll-kubectl-hpa-selected-name (car lll-kubectl-hpa-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-hpa-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "hpa"
		    lll-kubectl-hpa-selected-name
		    )))
  )

;; HPA definition -----------------------------------------------
(define-transient-command lll-kubectl-hpa-help ()
  "Help transient for lll kubectl hpa mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log hpa"	lll-kubectl-hpa-log)
   ("d" "Describe hpa"	lll-kubectl-hpa-describe)]
  )

(defvar lll-kubectl-hpa-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-hpa-help)
    (define-key map "l" 'lll-kubectl-hpa-log)
    (define-key map "d" 'lll-kubectl-hpa-describe)
    map)
  "Keymap for `lll-kubectl-hpa-mode'")

(defun lll-kubectl-hpa-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-hpa-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-hpa-entry (vconcat (split-string line)))
      	     (lll-hpa-entry-length (length lll-hpa-entry))
	     (lll-hpa-status (aref lll-hpa-entry 1)))
	(aset lll-hpa-entry 1
	      (propertize lll-hpa-status
			  'font-lock-face (lll-kubectl-hpa-status-face lll-hpa-status)))
      	(list (aref lll-hpa-entry 0) lll-hpa-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-hpa-entries ()
  "Return the kubectl hpas data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get hpa"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-hpa-parse lines)))

(defun lll-kubectl-hpa-refresh ()
  "Refresh kubectl hpa list"
  (setq tabulated-list-entries (lll-kubectl-hpa-entries)))

;; (defcustom lll-kubectl-hpa-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "REFERENCE")
;;                        (const "TARGETS")
;;                        (const "MINPODS")
;;                        (const "MAXPODS")
;;                        (const "REPLICAS")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-hpa-mode tabulated-list-mode
  "LLL kubectl hpas"
  "Major mode for handling hpas"
  (setq tabulated-list-format [("NAME" 25 t)
			       ("REFERENCE" 65 t)
			       ("TARGETS" 10 t)
			       ("MINPODS" 10 t)
			       ("MAXPODS" 10 t)
			       ("REPLICAS" 10 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-hpa-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-hpa ()
  "kubectl hpa list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-hpa*")
  (lll-kubectl-hpa-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-hpa-default-sort-key)
  (tabulated-list-revert))
;; HPA definition -----------------------------------------------

(provide 'lll-kubectl-hpa)

;;; lll-kubectl-hpa.el ends here
