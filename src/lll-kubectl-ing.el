;;; lll-kubectl.el --- ing file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-ing-selected ""
  "kubectl ing selected")

(defun lll-kubectl-ing-log ()
  "Log kubectl ing"
  (interactive)
  (let* ((lll-kubectl-ing-selected-list (tablist-get-marked-items))
	 (lll-kubectl-ing-selected (car lll-kubectl-ing-selected-list))
	 (lll-kubectl-ing-selected-name (car lll-kubectl-ing-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-ing-log*" nil
		    ;; (concat "lll-kubectl-ing-describe-" lll-kubectl-ing-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "ing/" lll-kubectl-ing-selected-name)
		    )))
  )

(defun lll-kubectl-ing-describe ()
  "Describe kubectl ing"
  (interactive)
  (let* ((lll-kubectl-ing-selected-list (tablist-get-marked-items))
	 (lll-kubectl-ing-selected (car lll-kubectl-ing-selected-list))
	 (lll-kubectl-ing-selected-name (car lll-kubectl-ing-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-ing-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "ing"
		    lll-kubectl-ing-selected-name
		    )))
  )

;; ING definition -----------------------------------------------
(define-transient-command lll-kubectl-ing-help ()
  "Help transient for lll kubectl ing mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log ing"	lll-kubectl-ing-log)
   ("d" "Describe ing"	lll-kubectl-ing-describe)]
  )

(defvar lll-kubectl-ing-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-ing-help)
    (define-key map "l" 'lll-kubectl-ing-log)
    (define-key map "d" 'lll-kubectl-ing-describe)
    map)
  "Keymap for `lll-kubectl-ing-mode'")

(defun lll-kubectl-ing-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-ing-parse (line-raw)
  "Convert a LINE to a `tabulated-list-entries' entry"
  ;; (string-match ", *" line)
  ;; (replace-match "," t t line)
  (setq line (replace-regexp-in-string ", *" "," line-raw))
  (setq line-entry (split-string line))
  (setq line-len (length line-entry))
  (setq missing-elems (- 6 line-len))
  (setq line-entry-full (append (butlast line-entry 2)
				(make-list missing-elems '"-")
				(nthcdr (- line-len 2) line-entry)))
  (setq line-entry-vector (vconcat line-entry-full))
  (list (aref line-entry-vector 0) line-entry-vector)
  ;; (condition-case nil
  ;;     (let* ((lll-ing-entry (split-string line))
  ;;     	     (lll-ing-entry-length (length lll-ing-entry))
  ;; 	     )
  ;; 	(setq missing-elems (- 6 lll-ing-entry-lenght))
  ;; 	(setq lll-ing-entry-vector
  ;; 	      (vconcat
  ;; 	       (append (butlast lll-ing-entry 2)
  ;; 		       (make-list missing-elems '"-")
  ;; 		       (nthcdr (- lll-ing-entry-length 2) lll-ing-entry))))
  ;; 	(setq lll-ing-status (aref lll-ing-entry-vector 1))
  ;; 	;; (aset lll-ing-entry-vector 1
  ;; 	;;       (propertize lll-ing-status
  ;; 	;; 		  'font-lock-face (lll-kubectl-ing-status-face lll-ing-status)))
  ;;     	(list (aref lll-ing-entry-vector 0) lll-ing-entry-vector))
  ;;   (error "Could not read string:\n%s" line))
  )

(defun lll-kubectl-ing-entries ()
  "Return the kubectl ings data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get ing"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t)))
	 )
    (-map #'lll-kubectl-ing-parse lines)))

(defun lll-kubectl-ing-refresh ()
  "Refresh kubectl ing list"
  (setq tabulated-list-entries (lll-kubectl-ing-entries)))

;; (defcustom lll-kubectl-ing-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "CLASS")
;;                        (const "HOSTS")
;;                        (const "ADDRESS")
;;                        (const "PORTS")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-ing-mode tabulated-list-mode
  "LLL kubectl ings"
  "Major mode for handling ings"
  (setq tabulated-list-format [("NAME" 25 t)
			       ("CLASS" 8 t)
			       ("HOSTS" 25 t)
			       ("ADDRESS" 10 t)
			       ("PORTS" 10 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-ing-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-ing ()
  "kubectl ing list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-ing*")
  (lll-kubectl-ing-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-ing-default-sort-key)
  (tabulated-list-revert))
;; ING definition -----------------------------------------------

(provide 'lll-kubectl-ing)

;;; lll-kubectl-ing.el ends here
