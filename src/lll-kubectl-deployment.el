;;; lll-kubectl.el --- deployment file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-deployment-selected ""
  "kubectl deployment selected")

(defun lll-kubectl-deployment-log ()
  "Log kubectl deployment"
  (interactive)
  (let* ((lll-kubectl-deployment-selected-list (tablist-get-marked-items))
	 (lll-kubectl-deployment-selected (car lll-kubectl-deployment-selected-list))
	 (lll-kubectl-deployment-selected-name (car lll-kubectl-deployment-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-deployment-log*" nil
		    ;; (concat "lll-kubectl-deployment-describe-" lll-kubectl-deployment-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "deployment/" lll-kubectl-deployment-selected-name)
		    )))
  )

(defun lll-kubectl-deployment-describe ()
  "Describe kubectl deployment"
  (interactive)
  (let* ((lll-kubectl-deployment-selected-list (tablist-get-marked-items))
	 (lll-kubectl-deployment-selected (car lll-kubectl-deployment-selected-list))
	 (lll-kubectl-deployment-selected-name (car lll-kubectl-deployment-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-deployment-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "deployment"
		    lll-kubectl-deployment-selected-name
		    )))
  )

(defun lll-kubectl-deployment-yaml ()
  "Get kubectl deployment yaml"
  (interactive)
  (let* ((lll-kubectl-deployment-selected-list (tablist-get-marked-items))
	 (lll-kubectl-deployment-selected (car lll-kubectl-deployment-selected-list))
	 (lll-kubectl-deployment-selected-name (car lll-kubectl-deployment-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-deployment-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "get" "deployment"
		    lll-kubectl-deployment-selected-name
		    "-o" "yaml"
		    )))
  )

;; DEPLOYMENT definition -----------------------------------------------
(define-transient-command lll-kubectl-deployment-help ()
  "Help transient for lll kubectl deployment mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log deployment"	lll-kubectl-deployment-log)
   ("d" "Describe deployment"	lll-kubectl-deployment-describe)
   ("y" "Get deployment yaml"	lll-kubectl-deployment-yaml)]
  )

(defvar lll-kubectl-deployment-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-deployment-help)
    (define-key map "l" 'lll-kubectl-deployment-log)
    (define-key map "d" 'lll-kubectl-deployment-describe)
    (define-key map "y" 'lll-kubectl-deployment-yaml)
    map)
  "Keymap for `lll-kubectl-deployment-mode'")

(defun lll-kubectl-deployment-status-face (status)
  "Return the correct face according to STATUS."
  (setq status-list (split-string status "/"))
  (if (string= (car status-list) (car (cdr status-list)))
      'lll-kubectl-face-status-up
    'default
    )
  )

(defun lll-kubectl-deployment-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-deployment-entry (vconcat (split-string line)))
      	     (lll-deployment-entry-length (length lll-deployment-entry))
	     (lll-deployment-status (aref lll-deployment-entry 1))
	     (lll-deployment-age (aref lll-deployment-entry 4))
	     )
	(aset lll-deployment-entry 1
	      (propertize lll-deployment-status
			  'font-lock-face
			  (lll-kubectl-deployment-status-face lll-deployment-status)))
	(aset lll-deployment-entry 4
	      (propertize (lll-kubectl-age2hours lll-deployment-age)
			  'font-lock-face
			  (lll-kubectl-deployment-status-face lll-deployment-status)))
      	(list (aref lll-deployment-entry 0) lll-deployment-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-deployment-entries ()
  "Return the kubectl deployments data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get deployments"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-deployment-parse lines)))

(defun lll-kubectl-deployment-refresh ()
  "Refresh kubectl deployment list"
  (setq tabulated-list-entries (lll-kubectl-deployment-entries)))

;; (defcustom lll-kubectl-deployment-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "READY")
;;                        (const "UP-TO-DATE")
;;                        (const "AVAILABLE")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-deployment-mode tabulated-list-mode
  "LLL kubectl deployments"
  "Major mode for handling deployments"
  (setq tabulated-list-format [("NAME" 65 t)
			       ("READY" 5 t)
			       ("UP-TO-DATE" 10 t)
			       ("AVAILABLE" 10 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-deployment-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-deployment ()
  "kubectl deployment list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-deployment*")
  (lll-kubectl-deployment-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-deployment-default-sort-key)
  (tabulated-list-revert))
;; DEPLOYMENT definition -----------------------------------------------

(provide 'lll-kubectl-deployment)

;;; lll-kubectl-deployment.el ends here
