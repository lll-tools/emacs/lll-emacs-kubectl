;;; lll-kubectl.el --- sts file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-sts-selected ""
  "kubectl sts selected")

(defun lll-kubectl-sts-log ()
  "Log kubectl sts"
  (interactive)
  (let* ((lll-kubectl-sts-selected-list (tablist-get-marked-items))
	 (lll-kubectl-sts-selected (car lll-kubectl-sts-selected-list))
	 (lll-kubectl-sts-selected-name (car lll-kubectl-sts-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-sts-log*" nil
		    ;; (concat "lll-kubectl-sts-describe-" lll-kubectl-sts-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "sts/" lll-kubectl-sts-selected-name)
		    )))
  )

(defun lll-kubectl-sts-describe ()
  "Describe kubectl sts"
  (interactive)
  (let* ((lll-kubectl-sts-selected-list (tablist-get-marked-items))
	 (lll-kubectl-sts-selected (car lll-kubectl-sts-selected-list))
	 (lll-kubectl-sts-selected-name (car lll-kubectl-sts-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-sts-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "sts"
		    lll-kubectl-sts-selected-name
		    )))
  )

;; STS definition -----------------------------------------------
(define-transient-command lll-kubectl-sts-help ()
  "Help transient for lll kubectl sts mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log sts"	lll-kubectl-sts-log)
   ("d" "Describe sts"	lll-kubectl-sts-describe)]
  )

(defvar lll-kubectl-sts-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-sts-help)
    (define-key map "l" 'lll-kubectl-sts-log)
    (define-key map "d" 'lll-kubectl-sts-describe)
    map)
  "Keymap for `lll-kubectl-sts-mode'")

(defun lll-kubectl-sts-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-sts-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-sts-entry (vconcat (split-string line)))
      	     (lll-sts-entry-length (length lll-sts-entry))
	     (lll-sts-status (aref lll-sts-entry 1)))
	(aset lll-sts-entry 1
	      (propertize lll-sts-status
			  'font-lock-face (lll-kubectl-sts-status-face lll-sts-status)))
      	(list (aref lll-sts-entry 0) lll-sts-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-sts-entries ()
  "Return the kubectl stss data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get sts"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-sts-parse lines)))

(defun lll-kubectl-sts-refresh ()
  "Refresh kubectl sts list"
  (setq tabulated-list-entries (lll-kubectl-sts-entries)))

;; (defcustom lll-kubectl-sts-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "READY")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-sts-mode tabulated-list-mode
  "LLL kubectl stss"
  "Major mode for handling stss"
  (setq tabulated-list-format [("NAME" 25 t)
			       ("READY" 8 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-sts-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-sts ()
  "kubectl sts list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-sts*")
  (lll-kubectl-sts-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-sts-default-sort-key)
  (tabulated-list-revert))
;; STS definition -----------------------------------------------

(provide 'lll-kubectl-sts)

;;; lll-kubectl-sts.el ends here
