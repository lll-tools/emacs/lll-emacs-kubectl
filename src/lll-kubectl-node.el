;;; lll-kubectl.el --- node file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-node-selected ""
  "kubectl node selected")

(defun lll-kubectl-node-log ()
  "Log kubectl node"
  (interactive)
  (let* ((lll-kubectl-node-selected-list (tablist-get-marked-items))
	 (lll-kubectl-node-selected (car lll-kubectl-node-selected-list))
	 (lll-kubectl-node-selected-name (car lll-kubectl-node-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-node-log*" nil
		    ;; (concat "lll-kubectl-node-describe-" lll-kubectl-node-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "node/" lll-kubectl-node-selected-name)
		    )))
  )

(defun lll-kubectl-node-describe ()
  "Describe kubectl node"
  (interactive)
  (let* ((lll-kubectl-node-selected-list (tablist-get-marked-items))
	 (lll-kubectl-node-selected (car lll-kubectl-node-selected-list))
	 (lll-kubectl-node-selected-name (car lll-kubectl-node-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-node-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "node"
		    lll-kubectl-node-selected-name
		    )))
  )

;; NODE definition -----------------------------------------------
(define-transient-command lll-kubectl-node-help ()
  "Help transient for lll kubectl node mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log node"	lll-kubectl-node-log)
   ("d" "Describe node"	lll-kubectl-node-describe)]
  )

(defvar lll-kubectl-node-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-node-help)
    (define-key map "l" 'lll-kubectl-node-log)
    (define-key map "d" 'lll-kubectl-node-describe)
    map)
  "Keymap for `lll-kubectl-node-mode'")

(defun lll-kubectl-node-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-node-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-node-entry (vconcat (split-string line)))
      	     (lll-node-entry-length (length lll-node-entry))
	     (lll-node-status (aref lll-node-entry 1)))
	(aset lll-node-entry 1
	      (propertize lll-node-status
			  'font-lock-face (lll-kubectl-node-status-face lll-node-status)))
      	(list (aref lll-node-entry 0) lll-node-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-node-entries ()
  "Return the kubectl nodes data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get nodes"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-node-parse lines)))

(defun lll-kubectl-node-refresh ()
  "Refresh kubectl node list"
  (setq tabulated-list-entries (lll-kubectl-node-entries)))

;; (defcustom lll-kubectl-node-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "STATUS")
;;                        (const "ROLES")
;;                        (const "AGE")
;;                        (const "VERSION")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-node-mode tabulated-list-mode
  "LLL kubectl nodes"
  "Major mode for handling nodes"
  (setq tabulated-list-format [("NAME" 25 t)
			       ("STATUS" 8 t)
			       ("ROLES" 25 t)
			       ("AGE" 5 t)
			       ("VERSION" 10 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-node-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-node ()
  "kubectl node list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-node*")
  (lll-kubectl-node-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-node-default-sort-key)
  (tabulated-list-revert))
;; NODE definition -----------------------------------------------

(provide 'lll-kubectl-node)

;;; lll-kubectl-node.el ends here
