;;; lll-kubectl.el --- allns file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defun lll-kubectl-allns-ingress ()
  "List ALL ingresses."
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-kubectl-allns-ingress*" nil
		  lll-kubectl-command nil
		  "get" "ingress" "-A"
		  ))
  )

(defun lll-kubectl-allns-service ()
  "List ALL services."
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-kubectl-allns-service*" nil
		  lll-kubectl-command nil
		  "get" "service" "-A"
		  ))
  )

(defun lll-kubectl-allns-deploy ()
  "List ALL deployments."
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-kubectl-allns-deploy*" nil
		  lll-kubectl-command nil
		  "get" "deploy" "-A"
		  ))
  )

(defun lll-kubectl-allns-secret ()
  "List ALL secrets."
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-kubectl-allns-secret*" nil
		  lll-kubectl-command nil
		  "get" "secret" "-A"
		  ))
  )

(defun lll-kubectl-allns-pod ()
  "List ALL pods."
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-kubectl-allns-pod*" nil
		  lll-kubectl-command nil
		  "get" "pod" "-A"
		  ))
  )

(defun lll-kubectl-allns-namespace ()
  "List kubectl namespaces"
  (interactive)
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-kubectl-allns-namespace*" nil
		  lll-kubectl-command nil
		  "get" "ns"
		  ))
  )

;;;###autoload
(define-transient-command lll-kubectl-allns ()
  "Transient for lll-kubectl ALL Namespaces."
  :man-page "lll-kubectl-allns"
  ["LLL kubectl ALL Namespaces"
   ("N" "List all namespaces"		lll-kubectl-allns-namespace)
   ("P" "List all pods"			lll-kubectl-allns-pod)
   ("S" "List all secrets"		lll-kubectl-allns-secret)
   ("D" "List all deployments"		lll-kubectl-allns-deploy)
   ("E" "List all services"		lll-kubectl-allns-service)
   ("I" "List all ingresses"		lll-kubectl-allns-ingress)
   ])

(provide 'lll-kubectl-allns)
