;;; lll-kubectl.el --- secret file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-secret-selected ""
  "kubectl secret selected")

(defun lll-kubectl-secret-keys-parse (line)
  "List all the keys and values for selected secret"
  (condition-case nil
      (let* ((command (concat lll-kubectl-command
			      " describe -n "
			      lll-kubectl-secret-selected
			      " "
			      line))
	     (lll-kubectl-secret-key-value
	      (replace-regexp-in-string "\n\\'" ""
		    (shell-command-to-string command))))
	(list line (vector
		    lll-kubectl-secret-selected
		    line lll-kubectl-secret-key-value)))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-secret-keys-entries ()
  "Return the kubectl secret keys data for `tabulated-list-entries' entry"
  (let* ((command (concat lll-kubectl-command
			  " list-keys "
			  lll-kubectl-secret-selected
			  ))
	 (data (shell-command-to-string command))
	 (lines (split-string data)))
    (-map #'lll-kubectl-secret-keys-parse lines)))

(defun lll-kubectl-secret-keys-refresh ()
  "Refresh kubectl secret keys list"
  (setq tabulated-list-entries (lll-kubectl-secret-keys-entries)))

(define-derived-mode lll-kubectl-secret-keys-mode tabulated-list-mode
  "Mode kubectl secret keys list"
  "Major mode for handling GTK secret keys"
  (setq tabulated-list-format [("Secret" 35 t)
			       ("Key" 25 t)
			       ("Value" 15 t)])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-secret-keys-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

(defun lll-kubectl-secret-keys ()
  "Show kubectl secret keys"
  (interactive)
  (let* ((lll-kubectl-secret-selected-list (tablist-get-marked-items))
	 (lll-kubectl-secret-selected (car lll-kubectl-secret-selected-list))
	 (lll-kubectl-secret-selected-name (car lll-kubectl-secret-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-secret-keys*" nil
		    lll-kubectl-command nil
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "get" "secret" lll-kubectl-secret-selected-name
		    "-o" "go-template='{{range $k,$v := .data}}{{$k}}{{\"\\n\"}}{{$v|base64decode}}{{\"\\n\"}}{{end}}'"
		    ;; (lll-kubectl-secret-keys-mode)
		    ;; (tabulated-list-revert))
		    )
		   )
    )
  )

(defun lll-kubectl-secret-log ()
  "Log kubectl secret"
  (interactive)
  (let* ((lll-kubectl-secret-selected-list (tablist-get-marked-items))
	 (lll-kubectl-secret-selected (car lll-kubectl-secret-selected-list))
	 (lll-kubectl-secret-selected-name (car lll-kubectl-secret-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-secret-log*" nil
		    ;; (concat "lll-kubectl-secret-describe-" lll-kubectl-secret-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    lll-kubectl-secret-selected-name
		    )))
  )

(defun lll-kubectl-secret-describe ()
  "Describe kubectl secret"
  (interactive)
  (let* ((lll-kubectl-secret-selected-list (tablist-get-marked-items))
	 (lll-kubectl-secret-selected (car lll-kubectl-secret-selected-list))
	 (lll-kubectl-secret-selected-name (car lll-kubectl-secret-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-secret-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "secret"
		    lll-kubectl-secret-selected-name
		    )))
  )

;; SECRET definition -----------------------------------------------
(define-transient-command lll-kubectl-secret-help ()
  "Help transient for lll kubectl secret mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("a" "List secret keys"	lll-kubectl-secret-keys)
   ("l" "Log secret"	lll-kubectl-secret-log)
   ("d" "Describe secret"	lll-kubectl-secret-describe)]
  )

(defvar lll-kubectl-secret-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-secret-help)
    (define-key map "a" 'lll-kubectl-secret-keys)
    (define-key map "l" 'lll-kubectl-secret-log)
    (define-key map "d" 'lll-kubectl-secret-describe)
    map)
  "Keymap for `lll-kubectl-secret-mode'")

(defun lll-kubectl-secret-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Running" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-secret-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* ((lll-secret-entry (vconcat (split-string line)))
      	     (lll-secret-entry-length (length lll-secret-entry))
	     (lll-secret-age (aref lll-secret-entry 3))
	     )
	(aset lll-secret-entry 3 (lll-kubectl-age2hours lll-secret-age))
      	(list (aref lll-secret-entry 0) lll-secret-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-secret-entries ()
  "Return the kubectl secrets data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get secrets"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-secret-parse lines)))

(defun lll-kubectl-secret-refresh ()
  "Refresh kubectl secret list."
  (setq tabulated-list-entries (lll-kubectl-secret-entries)))

;; (defcustom lll-kubectl-secret-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "TYPE")
;;                        (const "DATA")
;;                        (const "AGE"))
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-secret-mode tabulated-list-mode
  "LLL kubectl secrets"
  "Major mode for handling secrets"
  (setq tabulated-list-format [("NAME" 40 t)
			       ("TYPE" 40 t)
			       ("DATA" 5 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-secret-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-secret ()
  "kubectl secret list."
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-secret*")
  (lll-kubectl-secret-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-secret-default-sort-key)
  (tabulated-list-revert))
;; SECRET definition -----------------------------------------------

(provide 'lll-kubectl-secret)

;;; lll-kubectl-secret.el ends here
