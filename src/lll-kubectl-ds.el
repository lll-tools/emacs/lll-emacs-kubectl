;;; lll-kubectl.el --- ds file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-ds-selected ""
  "kubectl ds selected")

(defun lll-kubectl-ds-log ()
  "Log kubectl ds"
  (interactive)
  (let* ((lll-kubectl-ds-selected-list (tablist-get-marked-items))
	 (lll-kubectl-ds-selected (car lll-kubectl-ds-selected-list))
	 (lll-kubectl-ds-selected-name (car lll-kubectl-ds-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-ds-log*" nil
		    ;; (concat "lll-kubectl-ds-describe-" lll-kubectl-ds-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "ds/" lll-kubectl-ds-selected-name)
		    )))
  )

(defun lll-kubectl-ds-describe ()
  "Describe kubectl ds"
  (interactive)
  (let* ((lll-kubectl-ds-selected-list (tablist-get-marked-items))
	 (lll-kubectl-ds-selected (car lll-kubectl-ds-selected-list))
	 (lll-kubectl-ds-selected-name (car lll-kubectl-ds-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-ds-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "ds"
		    lll-kubectl-ds-selected-name
		    )))
  )

;; DS definition -----------------------------------------------
(define-transient-command lll-kubectl-ds-help ()
  "Help transient for lll kubectl ds mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log ds"	lll-kubectl-ds-log)
   ("d" "Describe ds"	lll-kubectl-ds-describe)]
  )

(defvar lll-kubectl-ds-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-ds-help)
    (define-key map "l" 'lll-kubectl-ds-log)
    (define-key map "d" 'lll-kubectl-ds-describe)
    map)
  "Keymap for `lll-kubectl-ds-mode'")

(defun lll-kubectl-ds-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-ds-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-ds-entry (vconcat (split-string line)))
      	     (lll-ds-entry-length (length lll-ds-entry))
	     (lll-ds-status (aref lll-ds-entry 1)))
	(aset lll-ds-entry 1
	      (propertize lll-ds-status
			  'font-lock-face (lll-kubectl-ds-status-face lll-ds-status)))
      	(list (aref lll-ds-entry 0) lll-ds-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-ds-entries ()
  "Return the kubectl dss data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get ds"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-ds-parse lines)))

(defun lll-kubectl-ds-refresh ()
  "Refresh kubectl ds list"
  (setq tabulated-list-entries (lll-kubectl-ds-entries)))

;; (defcustom lll-kubectl-ds-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "DESIRED")
;;                        (const "CURRENT")
;;                        (const "READY")
;;                        (const "UP-TO-DATE")
;;                        (const "AVAILABLE")
;;                        (const "NODE SELECTOR")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-ds-mode tabulated-list-mode
  "LLL kubectl dss"
  "Major mode for handling dss"
  (setq tabulated-list-format [("NAME" 40 t)
			       ("DESIRED" 8 t)
			       ("CURRENT" 8 t)
			       ("READY" 8 t)
			       ("UP-TO-DATE" 12 t)
			       ("AVAILABLE" 12 t)
			       ("NODE SELECTOR" 35 t)
			       ("AGE" 5 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-ds-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-ds ()
  "kubectl ds list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-ds*")
  (lll-kubectl-ds-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-ds-default-sort-key)
  (tabulated-list-revert))
;; DS definition -----------------------------------------------

(provide 'lll-kubectl-ds)

;;; lll-kubectl-ds.el ends here
