;;; lll-kubectl.el --- main file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Code:

(require 'transient)
(require 'dash)
;; https://github.com/magnars/s.el
;; (require 's)

(require 'lll-kubectl-config)
(require 'lll-kubectl-allns)
(require 'lll-kubectl-node)
(require 'lll-kubectl-secret)
(require 'lll-kubectl-pod)
(require 'lll-kubectl-deployment)
(require 'lll-kubectl-pv)
(require 'lll-kubectl-pvc)
(require 'lll-kubectl-rs)
(require 'lll-kubectl-ds)
(require 'lll-kubectl-sts)
(require 'lll-kubectl-svc)
(require 'lll-kubectl-ing)
(require 'lll-kubectl-hpa)
;; configmap
;; endpoints

(defgroup lll-kubectl nil
  "LLL kubectl customization group"
  :group 'convenience)

(defcustom lll-kubectl-command "kubectl"
  "LLL kubectl command"
  :group 'lll-kubectl
  :type 'string)

(defcustom lll-kubectl-kubeconfig "$HOME/.kube/config"
  "Kubeconfigfile"
  :group 'lll-kubectl
  :type 'string)

(defcustom lll-kubectl-arguments " -n kube-system"
  "Arguments for kubectl"
  :group 'lll-kubectl
  :type 'string)

(defface lll-kubectl-face-status-up
  '((t :inherit success))
  "Face used when the status is up."
  :group 'lll-kubectl)

(defface lll-kubectl-face-status-down
  '((t :inherit error))
  "Face used when the status is down"
  :group 'lll-kubectl)

(defface lll-kubectl-face-status-other
  '((t :inherit warning))
  "Face used when the status is not up/down."
  :group 'lll-kubectl)

(defun lll-kubectl-arguments ()
  "Return the latest used arguments in the `lll-kubectl' transient."
  (car (alist-get 'lll-kubectl transient-history)))

(defun lll-kubectl-run-command (&rest args)
  "Execute \"`lll-kubectl-command' ARGS\" and return the results."
  (let* ((flat-args (-remove 's-blank? (-flatten (list (lll-kubectl-arguments) args))))
         (command (s-join " " (-insert-at 0 lll-kubectl-command flat-args))))
    (message command)
    (s-trim-right (shell-command-to-string command))))

(defun lll-kubectl-age2hours (age)
  "Return the age in hours."
  ;; (setq age "1d1h")
  (if (not (string-match-p "d" age))
      (and (setq lll-kubectl-age-days "0")
	   (setq lll-kubectl-age-hours-text age))
    (setq lll-kubectl-age-list (split-string age "d"))
    (setq lll-kubectl-age-days (pop lll-kubectl-age-list))
    (setq lll-kubectl-age-hours-text (pop lll-kubectl-age-list)))

  (if (not (string-match-p "h" lll-kubectl-age-hours-text))
      (and (setq lll-kubectl-age-hours "0")
	   (setq lll-kubectl-age-minutes-text age))
    (setq lll-kubectl-age-list (split-string age "h"))
    (setq lll-kubectl-age-hours (pop lll-kubectl-age-list))
    (setq lll-kubectl-age-minutes-text (pop lll-kubectl-age-list)))

  ;; (message (format "%s days ; %s hours"
  ;; 		   lll-kubectl-age-days
  ;; 		   lll-kubectl-age-hours))
  (format "%06dh-(%s)"
	  (+ (* (string-to-number lll-kubectl-age-days) 24)
	     (string-to-number lll-kubectl-age-hours))
	  age)
  )

(defun lll-kubectl-get-marked-items-ids ()
  "Get the marked items from `tablist-get-marked-items'."
  (-map #'car (tablist-get-marked-items)))

(defun lll-kubectl-heading-actions ()
  "Heading actions."
  (let ((items (s-join ", " (lll-kubectl-get-marked-items-ids))))
    (format "%s (%s) %s (%s) %s %s"
            (propertize "Context" 'face 'transient-heading)
            (propertize lll-kubectl-config-context 'face 'transient-value)
            (propertize "Arguments" 'face 'transient-heading)
            (propertize lll-kubectl-arguments 'face 'transient-value)
            (propertize "Actions on" 'face 'transient-heading)
            (propertize items        'face 'transient-value)
	    )))

(defun lll-kubectl-heading ()
  "Show heading info."
  (setq lll-kubectl-config-context (lll-kubectl-run-command "config" "current-context"))
  (format "%s (%s) %s (%s)"
	  (propertize "LLL kubectl: Cluster" 'face 'transient-heading)
	  (propertize lll-kubectl-config-context 'face 'transient-value)
	  (propertize "Arguments" 'face 'font-lock-keyword-face)
	  (propertize lll-kubectl-arguments 'face 'font-lock-string-face))
)

(defun lll-kubectl-read-kbfile (prompt &optional initial-input _history)
  "Wrapper around `read-file-name'."
  (read-file-name prompt nil nil t initial-input (apply-partially 'string-match "config.*")))

;;;###autoload
(defun lll-kubectl-action ()
  "Action list"
  (interactive)
  (pop-to-buffer "*lll-kubectl-actions*")
  ;; (action-mode)
  (tabulated-list-revert))

;;;###autoload (autoload 'lll-kubectl "lll-kubectl" nil t)
(define-transient-command lll-kubectl ()
  "Transient for kubectl."
  :man-page "lll-kubectl"
  ["Arguments"
   ("-H"	"Host"		"--host "		read-string)
   ("-T"	"Template"     	"--template="		read-string)
   ("-K"	"Kubeconfig"	"--kubeconfig="		lll-kubectl-read-kbfile)
   ("-N"	"Namespace"	"-n "			read-string)
   ]
  [:description lll-kubectl-heading
   ("n" "Nodes"			lll-kubectl-node)
   ("p" "Pods"			lll-kubectl-pod)
   ("d" "Deployments"		lll-kubectl-deployment)
   ("e" "Services"	   	lll-kubectl-svc)
   ("s" "Secrets"      		lll-kubectl-secret)
   ("v" "Persistent Volumes"   	lll-kubectl-pv)
   ("l" "P. Volume Claims"   	lll-kubectl-pvc)
   ("r" "ReplicaSets"   	lll-kubectl-rs)
   ("m" "DaemonSets"	   	lll-kubectl-ds)
   ("f" "StatefulSets"	   	lll-kubectl-sts)
   ("i" "Ingress"	   	lll-kubectl-ing)
   ("h" "H. Pod Autoscalers"	lll-kubectl-hpa)
   ]
  ["Other"
   ("D" "Dashboard"		lll-kubectl-dash)
   ("C" "Cluster Config"	lll-kubectl-config)
   ("A" "ALL Nampespaces"	lll-kubectl-allns)
   ])

(provide 'lll-kubectl)

;;; lll-kubectl.el ends here
