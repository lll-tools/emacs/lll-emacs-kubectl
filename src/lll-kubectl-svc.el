;;; lll-kubectl.el --- svc file of lll-emacs-kubectl

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-kubectl
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

(defvar lll-kubectl-svc-selected ""
  "kubectl svc selected")

(defun lll-kubectl-svc-log ()
  "Log kubectl svc"
  (interactive)
  (let* ((lll-kubectl-svc-selected-list (tablist-get-marked-items))
	 (lll-kubectl-svc-selected (car lll-kubectl-svc-selected-list))
	 (lll-kubectl-svc-selected-name (car lll-kubectl-svc-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-svc-log*" nil
		    ;; (concat "lll-kubectl-svc-describe-" lll-kubectl-svc-selected-name) nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "logs" "-f"
		    (concat "svc/" lll-kubectl-svc-selected-name)
		    )))
  )

(defun lll-kubectl-svc-describe ()
  "Describe kubectl svc"
  (interactive)
  (let* ((lll-kubectl-svc-selected-list (tablist-get-marked-items))
	 (lll-kubectl-svc-selected (car lll-kubectl-svc-selected-list))
	 (lll-kubectl-svc-selected-name (car lll-kubectl-svc-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-kubectl-svc-describe*" nil
		    lll-kubectl-command nil
		    ;; TODO
		    (car (split-string lll-kubectl-arguments))
		    (car (cdr (split-string lll-kubectl-arguments)))
		    "describe" "svc"
		    lll-kubectl-svc-selected-name
		    )))
  )

;; SVC definition -----------------------------------------------
(define-transient-command lll-kubectl-svc-help ()
  "Help transient for lll kubectl svc mode"
  ["LLL kubectl help"
   ;; ("?" "Describe mode"		describe-mode)
   ("l" "Log svc"	lll-kubectl-svc-log)
   ("d" "Describe svc"	lll-kubectl-svc-describe)]
  )

(defvar lll-kubectl-svc-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-kubectl-svc-help)
    (define-key map "l" 'lll-kubectl-svc-log)
    (define-key map "d" 'lll-kubectl-svc-describe)
    map)
  "Keymap for `lll-kubectl-svc-mode'")

(defun lll-kubectl-svc-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-kubectl-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-kubectl-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-kubectl-face-status-down)
   (t
    'default)))

(defun lll-kubectl-svc-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry"
  (condition-case nil
      (let* ((lll-svc-entry (vconcat (split-string line)))
      	     (lll-svc-entry-length (length lll-svc-entry))
	     (lll-svc-status (aref lll-svc-entry 1))
	     (lll-svc-age (aref lll-svc-entry 5)))
	(aset lll-svc-entry 1
	      (propertize lll-svc-status
			  'font-lock-face (lll-kubectl-svc-status-face lll-svc-status)))
	(aset lll-svc-entry 5 (lll-kubectl-age2hours lll-svc-age))
      	(list (aref lll-svc-entry 0) lll-svc-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-kubectl-svc-entries ()
  "Return the kubectl svcs data for `tabulated-list-entries' entry"
  (let* (
	 (command (concat lll-kubectl-command lll-kubectl-arguments " get svc"))
	 (data (shell-command-to-string command))
	 (lines (cdr (split-string data "[\n\r]" t))))
    (-map #'lll-kubectl-svc-parse lines)))

(defun lll-kubectl-svc-refresh ()
  "Refresh kubectl svc list"
  (setq tabulated-list-entries (lll-kubectl-svc-entries)))

;; (defcustom lll-kubectl-svc-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-kubectl
;;   :type '(cons (choice (const "NAME")
;;                        (const "TYPE")
;;                        (const "CLUSTER-IP")
;;                        (const "EXTERNAL-IP")
;;                        (const "PORT(S)")
;;                        (const "AGE")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-kubectl-svc-mode tabulated-list-mode
  "LLL kubectl svcs"
  "Major mode for handling svcs"
  (setq tabulated-list-format [("NAME" 65 t)
			       ("TYPE" 18 t)
			       ("CLUSTER-IP" 18 t)
			       ("EXTERNAL-IP" 18 t)
			       ("PORT(S)" 18 t)
			       ("AGE" 18 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-kubectl-svc-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-kubectl-svc ()
  "kubectl svc list"
  (interactive)
  (when (lll-kubectl-arguments)
    (setq lll-kubectl-arguments "")
    (let ((arguments (lll-kubectl-arguments)))
      (while (> (length arguments) 0)
	(setq lll-kubectl-arguments (concat lll-kubectl-arguments " " (pop arguments))))
      ))

  (pop-to-buffer "*lll-kubectl-svc*")
  (lll-kubectl-svc-mode)
  ;; (setq tabulated-list-sort-key lll-kubectl-svc-default-sort-key)
  (tabulated-list-revert))
;; SVC definition -----------------------------------------------

(provide 'lll-kubectl-svc)

;;; lll-kubectl-svc.el ends here
